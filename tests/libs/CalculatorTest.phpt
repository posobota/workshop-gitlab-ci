<?php

namespace Test\Libs;

use App\Libs\Calculator;
use Tester\Assert;


require __DIR__ . '/../bootstrap.php';


final class CalculatorTest extends \Tester\TestCase
{
    public function testAdd()
    {
        $c = new Calculator();

        Assert::same(4, $c->add(2, 2));
    }
}


(new CalculatorTest())->run();
